//
//  AppDelegate.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 5/28/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

