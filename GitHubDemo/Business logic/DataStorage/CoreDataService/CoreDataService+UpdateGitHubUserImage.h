//
//  CoreDataService+UpdateGitHubUserImage.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/28/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "CoreDataService.h"

extern NSString * const kUpdateUserAvatarNotificationKey;

@class GitHubUser;

@interface CoreDataService (UpdateGitHubUserImage)

- (void)updateImageForUser:(GitHubUser *)user;

@end
