//
//  CoreDataService+UpdateGitHubUserImage.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/28/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "CoreDataService+UpdateGitHubUserImage.h"
#import "GitHubUser.h"
#import "CDGitHubUser+CoreDataClass.h"
#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>

NSString * const kUpdateUserAvatarNotificationKey = @"UpdateUserAvatarNotificationKey";

@implementation CoreDataService (UpdateGitHubUserImage)

- (void)updateImageForUser:(GitHubUser *)user {
    
    __weak CoreDataService *weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *imageURL = [NSURL URLWithString:user.avatarURLString];
        NSManagedObjectContext *context = weakSelf.backgroundContext;
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        GitHubUser *tempUser = [GitHubUser findByID:user.ID inContext:context];
        tempUser.cdGitHubUser.avatar = imageData;
        [weakSelf saveContext:context];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateUserAvatarNotificationKey object:@(user.ID)];
        
    });
}

@end
