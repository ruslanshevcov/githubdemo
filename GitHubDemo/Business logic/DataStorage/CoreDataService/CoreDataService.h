//
//  CoreDataService.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/26/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NSManagedObjectContext;

@interface CoreDataService : NSObject

@property (readonly) NSManagedObjectContext *backgroundContext;


- (void)saveContext:(NSManagedObjectContext *)context;

@end
