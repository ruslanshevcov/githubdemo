//
//  CoreDataService.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/26/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "CoreDataService.h"
#import <CoreData/CoreData.h>

@interface CoreDataService ()

@property (nonatomic, strong) NSPersistentContainer *persistentContainer;
@property (nonatomic, strong) NSManagedObjectContext *backgroundContext;

@end

@implementation CoreDataService

- (instancetype)init {
    if ([super init] == nil) {
        return nil;
    }
    
    self.backgroundContext = [self.persistentContainer newBackgroundContext];
    
    return self;
}

- (NSPersistentContainer *)persistentContainer {
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"GitHubDemoModel"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
            }];
        }
    }
    
    return _persistentContainer;
}

- (void)saveContext:(NSManagedObjectContext *)context {
    NSError *error = nil;
    @synchronized (context) {
        if ([context hasChanges]) {
            [context save:&error];
        }
    }
}

@end
