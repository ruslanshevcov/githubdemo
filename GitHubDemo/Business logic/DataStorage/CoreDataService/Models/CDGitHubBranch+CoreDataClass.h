//
//  CDGitHubBranch+CoreDataClass.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/26/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CDGitHubCommit, CDGitHubRepository;

NS_ASSUME_NONNULL_BEGIN

@interface CDGitHubBranch : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "CDGitHubBranch+CoreDataProperties.h"
