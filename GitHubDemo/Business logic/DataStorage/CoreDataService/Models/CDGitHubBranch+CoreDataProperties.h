//
//  CDGitHubBranch+CoreDataProperties.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/26/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//
//

#import "CDGitHubBranch+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface CDGitHubBranch (CoreDataProperties)

+ (NSFetchRequest<CDGitHubBranch *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *headCommitSha;
@property (nullable, nonatomic, retain) NSSet<CDGitHubCommit *> *commits;
@property (nullable, nonatomic, retain) CDGitHubRepository *repository;

@end

@interface CDGitHubBranch (CoreDataGeneratedAccessors)

- (void)addCommitsObject:(CDGitHubCommit *)value;
- (void)removeCommitsObject:(CDGitHubCommit *)value;
- (void)addCommits:(NSSet<CDGitHubCommit *> *)values;
- (void)removeCommits:(NSSet<CDGitHubCommit *> *)values;

@end

NS_ASSUME_NONNULL_END
