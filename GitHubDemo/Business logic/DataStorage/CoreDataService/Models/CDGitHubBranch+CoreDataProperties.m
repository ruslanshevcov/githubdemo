//
//  CDGitHubBranch+CoreDataProperties.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/26/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//
//

#import "CDGitHubBranch+CoreDataProperties.h"

@implementation CDGitHubBranch (CoreDataProperties)

+ (NSFetchRequest<CDGitHubBranch *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"CDGitHubBranch"];
}

@dynamic name;
@dynamic headCommitSha;
@dynamic commits;
@dynamic repository;

@end
