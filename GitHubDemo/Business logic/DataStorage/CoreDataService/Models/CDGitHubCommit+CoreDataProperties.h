//
//  CDGitHubCommit+CoreDataProperties.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/26/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//
//

#import "CDGitHubCommit+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface CDGitHubCommit (CoreDataProperties)

+ (NSFetchRequest<CDGitHubCommit *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *sha;
@property (nullable, nonatomic, copy) NSString *authorName;
@property (nullable, nonatomic, retain) CDGitHubUser *author;
@property (nullable, nonatomic, retain) CDGitHubBranch *branch;

@end

NS_ASSUME_NONNULL_END
