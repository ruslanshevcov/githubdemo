//
//  CDGitHubCommit+CoreDataProperties.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/26/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//
//

#import "CDGitHubCommit+CoreDataProperties.h"

@implementation CDGitHubCommit (CoreDataProperties)

+ (NSFetchRequest<CDGitHubCommit *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"CDGitHubCommit"];
}

@dynamic name;
@dynamic sha;
@dynamic authorName;
@dynamic author;
@dynamic branch;

@end
