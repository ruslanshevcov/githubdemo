//
//  CDGitHubRepository+CoreDataProperties.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/26/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//
//

#import "CDGitHubRepository+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface CDGitHubRepository (CoreDataProperties)

+ (NSFetchRequest<CDGitHubRepository *> *)fetchRequest;

@property (nonatomic) int32_t identification;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *desc;
@property (nullable, nonatomic, retain) CDGitHubUser *owner;
@property (nullable, nonatomic, retain) NSSet<CDGitHubBranch *> *branches;

@end

@interface CDGitHubRepository (CoreDataGeneratedAccessors)

- (void)addBranchesObject:(CDGitHubBranch *)value;
- (void)removeBranchesObject:(CDGitHubBranch *)value;
- (void)addBranches:(NSSet<CDGitHubBranch *> *)values;
- (void)removeBranches:(NSSet<CDGitHubBranch *> *)values;

@end

NS_ASSUME_NONNULL_END
