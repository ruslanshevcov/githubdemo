//
//  CDGitHubRepository+CoreDataProperties.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/26/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//
//

#import "CDGitHubRepository+CoreDataProperties.h"

@implementation CDGitHubRepository (CoreDataProperties)

+ (NSFetchRequest<CDGitHubRepository *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"CDGitHubRepository"];
}

@dynamic identification;
@dynamic name;
@dynamic desc;
@dynamic owner;
@dynamic branches;

@end
