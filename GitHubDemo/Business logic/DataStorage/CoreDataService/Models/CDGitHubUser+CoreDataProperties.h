//
//  CDGitHubUser+CoreDataProperties.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/26/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//
//

#import "CDGitHubUser+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface CDGitHubUser (CoreDataProperties)

+ (NSFetchRequest<CDGitHubUser *> *)fetchRequest;

@property (nonatomic) int32_t identification;
@property (nullable, nonatomic, copy) NSString *login;
@property (nullable, nonatomic, copy) NSString *avatarUrl;
@property (nullable, nonatomic, retain) NSData *avatar;
@property (nullable, nonatomic, retain) NSSet<CDGitHubRepository *> *repositories;
@property (nullable, nonatomic, retain) NSSet<CDGitHubCommit*> *commits;

@end

@interface CDGitHubUser (CoreDataGeneratedAccessors)

- (void)addRepositoriesObject:(CDGitHubRepository *)value;
- (void)removeRepositoriesObject:(CDGitHubRepository *)value;
- (void)addRepositories:(NSSet<CDGitHubRepository *> *)values;
- (void)removeRepositories:(NSSet<CDGitHubRepository *> *)values;

- (void)addCommitsObject:(CDGitHubCommit *)value;
- (void)removeCommitsObject:(CDGitHubCommit *)value;
- (void)addCommits:(NSSet<CDGitHubCommit *> *)values;
- (void)removeCommits:(NSSet<CDGitHubCommit *> *)values;

@end

NS_ASSUME_NONNULL_END
