//
//  CDGitHubUser+CoreDataProperties.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/26/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//
//

#import "CDGitHubUser+CoreDataProperties.h"

@implementation CDGitHubUser (CoreDataProperties)

+ (NSFetchRequest<CDGitHubUser *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"CDGitHubUser"];
}

@dynamic identification;
@dynamic login;
@dynamic avatarUrl;
@dynamic avatar;
@dynamic repositories;
@dynamic commits;

@end
