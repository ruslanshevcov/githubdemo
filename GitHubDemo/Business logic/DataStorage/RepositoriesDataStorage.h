//
//  RepositoriesDataStorage.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/21/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GitHubRepository.h"

@class CoreDataService;
@class GitHubRepository;

@interface RepositoriesDataStorage : NSObject

@property (readonly) CoreDataService *coreDataService;
@property (readonly) NSArray<GitHubRepository *> *repositories;

typedef void (^RepositoriesDataStorageCompletion)(void);
- (void)getRepositoriesWithCompletion:(RepositoriesDataStorageCompletion)completion;

- (void)refresh;

@end
