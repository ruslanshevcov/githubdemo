//
//  RepositoriesDataStorage.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/21/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "RepositoriesDataStorage.h"
#import "GitHubApiRepositoriesRequest.h"
#import "GitHubRepositoriesApiResponse.h"
#import "GitHubApiService.h"
#import "CoreDataService.h"
#import "CoreDataService+UpdateGitHubUserImage.h"

@interface RepositoriesDataStorage ()

@property (nonatomic, strong) GitHubApiService *apiService;
@property (nonatomic, strong) GitHubApiRepositoriesRequest *request;
@property (nonatomic, strong) NSArray<GitHubRepository *> *repositories;
@property (nonatomic, strong) CoreDataService *coreDataService;

@end

@implementation RepositoriesDataStorage

- (instancetype)init {
    if ([super init] == nil) {
        return nil;
    }
    
    self.apiService = [GitHubApiService new];
    self.coreDataService = [CoreDataService new];
    self.repositories = [GitHubRepository repositoriesInContext:self.coreDataService.backgroundContext];
    
    return self;
}

- (void)getRepositoriesWithCompletion:(RepositoriesDataStorageCompletion)completion {
    
    if (self.request == nil) {
        self.request = [GitHubApiRepositoriesRequest new];
    }
    
    __weak RepositoriesDataStorage *weakSelf = self;
    self.request.completion = ^(GitHubApiResponse *response, NSError *__autoreleasing *error) {
        
        if ([response isKindOfClass:[GitHubRepositoriesApiResponse class]]) {
            
            NSManagedObjectContext *context = weakSelf.coreDataService.backgroundContext;
            
            [((GitHubRepositoriesApiResponse *)response).repositories enumerateObjectsUsingBlock:^(GitHubRepository * _Nonnull repository, NSUInteger idx, BOOL * _Nonnull stop) {
                [GitHubRepository updateRepository:repository inContext:context];
                [weakSelf.coreDataService updateImageForUser:repository.owner];
            }];
            
            [weakSelf.coreDataService saveContext:context];
            
            weakSelf.repositories = [GitHubRepository repositoriesInContext:context];
        }
        
        completion();
    };
    
    [self.apiService doRequest:self.request];
}

- (void)refresh {

    // TODO: check internet connection
    
    NSManagedObjectContext *context = self.coreDataService.backgroundContext;
    
    [self.repositories enumerateObjectsUsingBlock:^(GitHubRepository * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [GitHubRepository deleteRepository:obj inContext:context];
    }];
    
    // TODO: delete all users
    
    [self.coreDataService saveContext:context];
    
    self.request = nil;
    self.repositories = [GitHubRepository repositoriesInContext:self.coreDataService.backgroundContext];
}

@end
