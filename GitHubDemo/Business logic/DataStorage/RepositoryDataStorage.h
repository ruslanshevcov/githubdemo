//
//  RepositoryDataStorage.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/24/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GitHubRepository;
@class CoreDataService;

@interface RepositoryDataStorage : NSObject

@property (readonly) GitHubRepository *repository;

- (instancetype)initWith:(GitHubRepository *)repository coreDataService:(CoreDataService *)coreDataService;

typedef void (^RepositoryDataStorageCompletion)(void);
- (void)getCommitsWithCompletion:(RepositoryDataStorageCompletion)completion;

@end
