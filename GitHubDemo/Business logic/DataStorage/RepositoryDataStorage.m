//
//  RepositoryDataStorage.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/24/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "RepositoryDataStorage.h"
#import "GitHubApiService.h"
#import "GitHubCommitsApiRequest.h"
#import "GitHubCommitsApiResponse.h"
#import "GitHubBranchesApiRequest.h"
#import "GitHubBranchesApiResponse.h"
#import "GitHubBranch.h"
#import "GitHubRepository.h"
#import "GitHubCommit.h"
#import "CoreDataService.h"
#import "CoreDataService+UpdateGitHubUserImage.h"

@interface RepositoryDataStorage ()

@property (nonatomic, strong) GitHubRepository *repository;
@property (nonatomic, strong) GitHubApiService *apiService;
@property (nonatomic, strong) GitHubBranchesApiRequest *branchesRequest;
@property (nonatomic, strong) CoreDataService *coreDataService;

@property (nonatomic, copy) RepositoryDataStorageCompletion completion;

@end

@implementation RepositoryDataStorage

- (instancetype)initWith:(GitHubRepository *)repository coreDataService:(CoreDataService *)coreDataService {
    if ([super init] == nil) {
        return nil;
    }
    
    self.repository = repository;
    self.apiService = [GitHubApiService new];
    self.coreDataService = coreDataService;
    
    return self;
}

- (void)getCommitsWithCompletion:(RepositoryDataStorageCompletion)completion {
    
    if (self.repository.branches.count > 0) {
        [self getCommits];
        return;
    }
    
    self.completion = completion;
    
    if (self.branchesRequest == nil) {
        self.branchesRequest = [[GitHubBranchesApiRequest alloc] initWith:self.repository];
    }
    
    __weak RepositoryDataStorage *weakSelf = self;
    self.branchesRequest.completion = ^(GitHubApiResponse *response, NSError *__autoreleasing *error) {
        if ([response isKindOfClass:[GitHubBranchesApiResponse class]]) {
            
            NSManagedObjectContext *context = weakSelf.coreDataService.backgroundContext;
            
            [((GitHubBranchesApiResponse *)response).branches enumerateObjectsUsingBlock:^(GitHubBranch * _Nonnull branch, NSUInteger idx, BOOL * _Nonnull stop) {
                [GitHubBranch updateBranch:branch inContext:context];
            }];
            
            [weakSelf.coreDataService saveContext:context];
            
            weakSelf.repository = [GitHubRepository findByID:weakSelf.repository.ID inContext:context];
            
            [weakSelf getCommits];
        }
    };
    
    [self.apiService doRequest:self.branchesRequest];
}

- (void)getCommits {
    
    __weak RepositoryDataStorage *weakSelf = self;
    [self.repository.branches enumerateObjectsUsingBlock:^(GitHubBranch * _Nonnull branch, NSUInteger idx, BOOL * _Nonnull stop) {
        GitHubCommitsApiRequest *commitsRequest = [[GitHubCommitsApiRequest alloc] initWith:self.repository forBranch:branch];
        
        commitsRequest.completion = ^(GitHubApiResponse *response, NSError *__autoreleasing *error) {
            
            NSManagedObjectContext *context = weakSelf.coreDataService.backgroundContext;
            
            if ([response isKindOfClass:[GitHubCommitsApiResponse class]]) {
                
                [((GitHubCommitsApiResponse *)response).commits enumerateObjectsUsingBlock:^(GitHubCommit * _Nonnull commit, NSUInteger idx, BOOL * _Nonnull stop) {
                    [GitHubCommit updateCommit:commit inContext:context];
                    [weakSelf.coreDataService updateImageForUser:commit.author];
                }];
                
                [weakSelf.coreDataService saveContext:context];
                
                weakSelf.repository = [GitHubRepository findByID:weakSelf.repository.ID inContext:context];
                
                if (weakSelf.completion != nil) {
                    weakSelf.completion();
                }
            }
        };
        
        [self.apiService doRequest:commitsRequest];
    }];
}

@end
