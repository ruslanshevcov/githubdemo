//
//  GitHubBranch.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/25/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GitHubCommit;
@class NSManagedObjectContext;
@class CDGitHubBranch;
@class GitHubRepository;

@interface GitHubBranch : NSObject

@property (readonly) NSString *name;
@property (readonly) NSString *headCommitSha;
@property (readonly) NSArray<GitHubCommit *> *commits;
@property (readonly) CDGitHubBranch *cdGitHubBranch;
@property (readonly) GitHubRepository *repository;

- (instancetype)initWithJSONData:(NSDictionary *)jsonDictionary forRepository:(GitHubRepository *)repository;

- (void)updateCommits:(NSArray<GitHubCommit *> *)commits;

#pragma mark -
#pragma mark CoreData

- (instancetype)initWithCDGitHubBranch:(CDGitHubBranch *)cdGitHubBranch forRepository:(GitHubRepository *)repository;
+ (void)updateBranch:(GitHubBranch *)branch inContext:(NSManagedObjectContext *)context;
+ (void)deleteBranch:(GitHubBranch *)branch inContext:(NSManagedObjectContext *)context;
+ (GitHubBranch *)findByRepository:(GitHubRepository *)repository name:(NSString *)name inContext:(NSManagedObjectContext *)context;

@end
