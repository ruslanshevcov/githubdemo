//
//  GitHubBranch.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/25/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "GitHubBranch.h"
#import "CDGitHubBranch+CoreDataClass.h"
#import "NSObject+ClassName.h"
#import "GitHubRepository.h"
#import "CDGitHubRepository+CoreDataClass.h"
#import "GitHubCommit.h"

static NSString *const kSHAKey = @"sha";
static NSString *const kCommitKey = @"commit";
static NSString *const kNameKey = @"name";

@interface GitHubBranch ()

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *headCommitSha;
@property (nonatomic, strong) NSArray<GitHubCommit *> *commits;
@property (nonatomic, strong) CDGitHubBranch *cdGitHubBranch;
@property (nonatomic, strong) GitHubRepository *repository;

@end

@implementation GitHubBranch

- (instancetype)initWithJSONData:(NSDictionary *)jsonDictionary forRepository:(GitHubRepository *)repository {
    if ([super init] == nil) {
        return nil;
    }
    
    if ([jsonDictionary isKindOfClass:[NSNull class]]) {
        return nil;
    }
    
    self.name = jsonDictionary[kNameKey];
    self.headCommitSha = jsonDictionary[kCommitKey][kSHAKey];
    self.repository = repository;
    
    return self;
}

- (void)updateCommits:(NSArray<GitHubCommit *> *)commits {
    self.commits = [NSArray<GitHubCommit *> arrayWithArray:commits];
}

#pragma mark -
#pragma mark CoreData

- (instancetype)initWithCDGitHubBranch:(CDGitHubBranch *)cdGitHubBranch forRepository:(GitHubRepository *)repository {
    if ([super init] == nil) {
        return nil;
    }
    
    self.cdGitHubBranch = cdGitHubBranch;
    
    self.name = cdGitHubBranch.name;
    self.headCommitSha = cdGitHubBranch.headCommitSha;
    self.repository = repository;
    
    __block NSMutableArray<GitHubCommit *> *commits = [NSMutableArray<GitHubCommit *> array];
    
    [cdGitHubBranch.commits enumerateObjectsUsingBlock:^(CDGitHubCommit * _Nonnull commit, BOOL * _Nonnull stop) {
        [commits addObject:[[GitHubCommit alloc] initWithCDGitHubCommit:commit forBranch:self]];
    }];
    
    self.commits = [NSArray<GitHubCommit *> arrayWithArray:commits];
    
    return self;
}

+ (void)updateBranch:(GitHubBranch *)branch inContext:(NSManagedObjectContext *)context; {
    
    CDGitHubBranch *cdBranch = [self findByRepository:branch.repository name:branch.name inContext:context].cdGitHubBranch;
    
    if (cdBranch == nil) {
        cdBranch = [NSEntityDescription insertNewObjectForEntityForName:[CDGitHubBranch className] inManagedObjectContext:context];
    }
    
    cdBranch.name = branch.name;
    cdBranch.headCommitSha = branch.headCommitSha;
    cdBranch.repository = [GitHubRepository findByID:branch.repository.ID inContext:context].cdGitHubRepository;
    [cdBranch.repository addBranchesObject:cdBranch];
    
    branch.cdGitHubBranch = cdBranch;
    
    // TODO: update commits
}

+ (void)deleteBranch:(GitHubBranch *)branch inContext:(NSManagedObjectContext *)context {
    if (branch.cdGitHubBranch != nil) {
        [context deleteObject:branch.cdGitHubBranch];
    }
}

+ (GitHubBranch *)findByRepository:(GitHubRepository *)repository name:(NSString *)name inContext:(NSManagedObjectContext *)context {
    return [[self findBy:[NSPredicate predicateWithFormat:@"name == %@ && repository.identification == %d", name, (int)repository.ID] forRepository:repository inContext:context] firstObject];
}

+ (NSArray<GitHubBranch *> *)findBy:(NSPredicate *)predicate forRepository:(GitHubRepository *)repository inContext:(NSManagedObjectContext *)context {
    NSFetchRequest *request = [CDGitHubBranch fetchRequest];
    request.predicate = predicate;
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    
    __block NSMutableArray<GitHubBranch *> *branches = [NSMutableArray<GitHubBranch *> array];
    [results enumerateObjectsUsingBlock:^(CDGitHubBranch *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [branches addObject:[[GitHubBranch alloc] initWithCDGitHubBranch:obj forRepository:repository]];
    }];
    
    return [NSArray<GitHubBranch *> arrayWithArray:branches];
}

@end
