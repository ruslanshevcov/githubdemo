//
//  GitHubCommit.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/25/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GitHubUser;
@class GitHubBranch;
@class CDGitHubCommit;
@class NSManagedObjectContext;

@interface GitHubCommit : NSObject

@property (readonly) NSString *name;
@property (readonly) NSString *sha;
@property (readonly) GitHubUser *author;
@property (readonly) NSString *authorName;
@property (readonly) GitHubBranch *branch;

- (instancetype)initWithJSONData:(NSDictionary *)jsonDictionary forBranch:(GitHubBranch *)branch;

#pragma mark -
#pragma mark CoreData

- (instancetype)initWithCDGitHubCommit:(CDGitHubCommit *)cdGitHubCommit forBranch:(GitHubBranch *)branch;
+ (void)updateCommit:(GitHubCommit *)commit inContext:(NSManagedObjectContext *)context;
+ (void)deleteCommit:(GitHubCommit *)commit inContext:(NSManagedObjectContext *)context;

@end
