//
//  GitHubCommit.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/25/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "GitHubCommit.h"
#import "GitHubUser.h"
#import "GitHubBranch.h"
#import "CDGitHubCommit+CoreDataClass.h"
#import "NSObject+ClassName.h"
#import "CDGitHubBranch+CoreDataClass.h"

static NSString *const kSHAKey = @"sha";
static NSString *const kAuthorKey = @"author";
static NSString *const kCommitKey = @"commit";
static NSString *const kMessageKey = @"message";
static NSString *const kNameKey = @"name";

@interface GitHubCommit ()

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *sha;
@property (nonatomic, strong) GitHubUser *author;
@property (nonatomic, strong) NSString *authorName;
@property (nonatomic, strong) GitHubBranch *branch;
@property (nonatomic, strong) CDGitHubCommit *cdGitHubCommit;

@end

@implementation GitHubCommit

- (instancetype)initWithJSONData:(NSDictionary *)jsonDictionary forBranch:(GitHubBranch *)branch {
    if ([super init] == nil) {
        return nil;
    }
    
    if ([jsonDictionary isKindOfClass:[NSNull class]]) {
        return nil;
    }
    
    self.sha = jsonDictionary[kSHAKey];
    self.author = [[GitHubUser alloc] initWithJSONData: jsonDictionary[kAuthorKey]];
    self.name = jsonDictionary[kCommitKey][kMessageKey];
    self.authorName = jsonDictionary[kCommitKey][kAuthorKey][kNameKey];
    
    self.branch = branch;
    
    return self;
}

#pragma mark -
#pragma mark CoreData

- (instancetype)initWithCDGitHubCommit:(CDGitHubCommit *)cdGitHubCommit forBranch:(GitHubBranch *)branch {
    if ([super init] == nil) {
        return nil;
    }
    
    self.cdGitHubCommit = cdGitHubCommit;
    
    self.name = cdGitHubCommit.name;
    self.sha = cdGitHubCommit.sha;
    self.authorName = cdGitHubCommit.authorName;
    self.author = [[GitHubUser alloc] initWithCDGitHubUser:cdGitHubCommit.author];
    self.branch = branch;
    
    return self;
}

+ (void)updateCommit:(GitHubCommit *)commit inContext:(NSManagedObjectContext *)context {
    
    CDGitHubCommit *cdCommit = [self findByBranch:commit.branch sha:commit.sha inContext:context].cdGitHubCommit;
    
    if (cdCommit == nil) {
        cdCommit = [NSEntityDescription insertNewObjectForEntityForName:[CDGitHubCommit className] inManagedObjectContext:context];
    }
    
    cdCommit.name = commit.name;
    cdCommit.sha = commit.sha;
    cdCommit.authorName = commit.authorName;
 
    [GitHubUser updateUser:commit.author inContext:context];
    
    cdCommit.author = commit.author.cdGitHubUser;
    
    cdCommit.branch = [GitHubBranch findByRepository:commit.branch.repository name:commit.branch.name inContext:context].cdGitHubBranch;
    [cdCommit.branch addCommitsObject:cdCommit];
    
    commit.cdGitHubCommit = cdCommit;
}

+ (void)deleteCommit:(GitHubCommit *)commit inContext:(NSManagedObjectContext *)context {
    if (commit.cdGitHubCommit != nil) {
        [context deleteObject:commit.cdGitHubCommit];
    }
}

+ (GitHubCommit *)findByBranch:(GitHubBranch *)branch sha:(NSString *)sha inContext:(NSManagedObjectContext *)context {
    return [[self findBy:[NSPredicate predicateWithFormat:@"sha == %@", sha] forBranch:branch inContext:context] firstObject];
}

+ (NSArray<GitHubCommit *> *)findBy:(NSPredicate *)predicate forBranch:(GitHubBranch *)branch inContext:(NSManagedObjectContext *)context {
    NSFetchRequest *request = [CDGitHubCommit fetchRequest];
    request.predicate = predicate;
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    
    __block NSMutableArray<GitHubCommit *> *commits = [NSMutableArray<GitHubCommit *> array];
    [results enumerateObjectsUsingBlock:^(CDGitHubCommit *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [commits addObject:[[GitHubCommit alloc] initWithCDGitHubCommit:obj forBranch:branch]];
    }];
    
    return [NSArray<GitHubCommit *> arrayWithArray:commits];
}

@end
