//
//  GitHubRepository.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/21/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GitHubUser;
@class GitHubBranch;
@class CDGitHubRepository;
@class NSManagedObjectContext;

@interface GitHubRepository : NSObject

@property (readonly) NSInteger ID;
@property (readonly) NSString *name;
@property (readonly) GitHubUser *owner;
@property (readonly) NSString *desc;
@property (readonly) NSArray<GitHubBranch *> *branches;
@property (readonly) CDGitHubRepository *cdGitHubRepository;

- (instancetype)initWithJSONData:(NSDictionary *)jsonDictionary;

- (void)updateBranches:(NSArray<GitHubBranch *> *)branches;

#pragma mark -
#pragma mark CoreData

- (instancetype)initWithCDGitHubRepository:(CDGitHubRepository *)cdGitHubRepository;
+ (void)updateRepository:(GitHubRepository *)repository inContext:(NSManagedObjectContext *)context;
+ (NSArray<GitHubRepository *> *)repositoriesInContext:(NSManagedObjectContext *)context;
+ (void)deleteRepository:(GitHubRepository *)repository inContext:(NSManagedObjectContext *)context;
+ (GitHubRepository *)findByID:(NSInteger)ID inContext:(NSManagedObjectContext *)context;

@end
