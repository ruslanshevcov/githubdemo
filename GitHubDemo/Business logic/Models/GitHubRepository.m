//
//  GitHubRepository.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/21/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "GitHubRepository.h"
#import "GitHubUser.h"
#import "GitHubBranch.h"
#import "CDGitHubRepository+CoreDataClass.h"
#import <CoreData/CoreData.h>
#import "NSObject+ClassName.h"
#import "CoreDataService.h"
#import "Macro.h"

static NSString *const kIdKey = @"id";
static NSString *const kNameKey = @"name";
static NSString *const kOwnerKey = @"owner";
static NSString *const kDescriptionKey = @"description";

@interface GitHubRepository ()

@property (nonatomic, assign) NSInteger ID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) GitHubUser *owner;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSArray<GitHubBranch *> *branches;
@property (nonatomic, strong) CDGitHubRepository *cdGitHubRepository;

@end

@implementation GitHubRepository

- (instancetype)initWithJSONData:(NSDictionary *)jsonDictionary {
    if ([super init] == nil) {
        return nil;
    }
    
    if ([jsonDictionary isKindOfClass:[NSNull class]]) {
        return nil;
    }
    
    self.ID = [jsonDictionary[kIdKey] integerValue];
    self.name = jsonDictionary[kNameKey];
    self.owner = [[GitHubUser alloc] initWithJSONData: jsonDictionary[kOwnerKey]];
    self.desc = CHECK_NSNULL(jsonDictionary[kDescriptionKey]);
    
    return self;
}

- (void)updateBranches:(NSArray<GitHubBranch *> *)branches {
    self.branches = [NSArray arrayWithArray:branches];
}

#pragma mark -
#pragma mark CoreData

- (instancetype)initWithCDGitHubRepository:(CDGitHubRepository *)cdGitHubRepository {
    if ([super init] == nil) {
        return nil;
    }
    
    self.cdGitHubRepository = cdGitHubRepository;
    
    self.ID = cdGitHubRepository.identification;
    self.name = cdGitHubRepository.name;
    self.desc = cdGitHubRepository.desc;
    
    self.owner = [[GitHubUser alloc] initWithCDGitHubUser:cdGitHubRepository.owner];
    
    __block NSMutableArray<GitHubBranch *> *branches = [NSMutableArray<GitHubBranch *> array];
    
    [cdGitHubRepository.branches enumerateObjectsUsingBlock:^(CDGitHubBranch * _Nonnull branch, BOOL * _Nonnull stop) {
        [branches addObject:[[GitHubBranch alloc] initWithCDGitHubBranch:branch forRepository:self]];
    }];
    
    self.branches = [NSArray<GitHubBranch *> arrayWithArray:branches];
    
    return self;
}

+ (void)updateRepository:(GitHubRepository *)repository inContext:(NSManagedObjectContext *)context {

    CDGitHubRepository *cdRepository = [self findByID:repository.ID inContext:context].cdGitHubRepository;
    
    if (cdRepository == nil) {
        cdRepository = [NSEntityDescription insertNewObjectForEntityForName:[CDGitHubRepository className] inManagedObjectContext:context];
    }
    
    cdRepository.identification = (int)repository.ID;
    cdRepository.name = repository.name;
    cdRepository.desc = repository.desc;
    
    [GitHubUser updateUser:repository.owner inContext:context];
    
    cdRepository.owner = repository.owner.cdGitHubUser;
    
    repository.cdGitHubRepository = cdRepository;
    
    // TODO: update branches
}

+ (void)deleteRepository:(GitHubRepository *)repository inContext:(NSManagedObjectContext *)context {
    if (repository.cdGitHubRepository != nil) {
        [context deleteObject:repository.cdGitHubRepository];
    }
}

+ (NSArray<GitHubRepository *> *)repositoriesInContext:(NSManagedObjectContext *)context {
    return [self findBy:nil inContext:context];
}

+ (GitHubRepository *)findByID:(NSInteger)ID inContext:(NSManagedObjectContext *)context {
    return [[self findBy:[NSPredicate predicateWithFormat:@"identification == %d", (int)ID] inContext:context] firstObject];
}

+ (NSArray<GitHubRepository *> *)findBy:(NSPredicate *)predicate inContext:(NSManagedObjectContext *)context {
    NSFetchRequest *request = [CDGitHubRepository fetchRequest];
    request.predicate = predicate;
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    
    __block NSMutableArray<GitHubRepository *> *repositories = [NSMutableArray<GitHubRepository *> array];
    [results enumerateObjectsUsingBlock:^(CDGitHubRepository *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [repositories addObject:[[GitHubRepository alloc] initWithCDGitHubRepository:obj]];
    }];
    
    return [NSArray<GitHubRepository *> arrayWithArray:repositories];
}

@end
