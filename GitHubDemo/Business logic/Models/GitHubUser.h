//
//  GitHubUser.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/21/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Observable.h"

@class NSManagedObjectContext;
@class CDGitHubUser;

@interface GitHubUser : NSObject

@property (readonly) NSInteger ID;
@property (readonly) NSString *login;
@property (readonly) NSString *avatarURLString;
@property (readonly) CDGitHubUser *cdGitHubUser;
@property (readonly) Observable<UIImage *> *avatarImage;

- (instancetype)initWithJSONData:(NSDictionary *)jsonDictionary;

#pragma mark -
#pragma mark CoreData

- (instancetype)initWithCDGitHubUser:(CDGitHubUser *)cdGitHubUser;
+ (void)updateUser:(GitHubUser *)user inContext:(NSManagedObjectContext *)context;
+ (NSArray<GitHubUser *> *)repositoriesInContext:(NSManagedObjectContext *)context;
+ (void)deleteUser:(GitHubUser *)user inContext:(NSManagedObjectContext *)context;
+ (GitHubUser *)findByID:(NSInteger)ID inContext:(NSManagedObjectContext *)context;

@end
