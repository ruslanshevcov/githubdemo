//
//  GitHubUser.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/21/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "GitHubUser.h"
#import "CDGitHubUser+CoreDataClass.h"
#import <CoreData/CoreData.h>
#import "NSObject+ClassName.h"
#import "CoreDataService+UpdateGitHubUserImage.h"

static NSString *const kIdKey = @"id";
static NSString *const kLoginKey = @"login";
static NSString *const kAvatarURLKey = @"avatar_url";

@interface GitHubUser ()

@property (nonatomic, assign) NSInteger ID;
@property (nonatomic, strong) NSString *login;
@property (nonatomic, strong) NSString *avatarURLString;
@property (nonatomic, strong) CDGitHubUser *cdGitHubUser;
@property (nonatomic, strong) Observable<UIImage *> *avatarImage;

@end

@implementation GitHubUser

- (instancetype)initWithJSONData:(NSDictionary *)jsonDictionary {
    if ([super init] == nil) {
        return nil;
    }
    
    if ([jsonDictionary isKindOfClass:[NSNull class]]) {
        return nil;
    }
    
    self.ID = [jsonDictionary[kIdKey] integerValue];
    self.login = jsonDictionary[kLoginKey];
    self.avatarURLString = jsonDictionary[kAvatarURLKey];
    
    return self;
}

#pragma mark -
#pragma mark CoreData

- (instancetype)initWithCDGitHubUser:(CDGitHubUser *)cdGitHubUser {
    if ([super init] == nil) {
        return nil;
    }
    
    self.cdGitHubUser = cdGitHubUser;
    
    self.ID = cdGitHubUser.identification;
    self.login = cdGitHubUser.login;
    self.avatarURLString = cdGitHubUser.avatarUrl;
    self.avatarImage = [Observable<UIImage *> new];
    self.avatarImage.value = [UIImage imageWithData:cdGitHubUser.avatar];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveUpdateAvatarNotification:) name:kUpdateUserAvatarNotificationKey object:nil];
    
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (void)updateUser:(GitHubUser *)user inContext:(NSManagedObjectContext *)context {
    
    CDGitHubUser *cdUser = [self findByID:user.ID inContext:context].cdGitHubUser;
    
    if (cdUser == nil) {
        cdUser = [NSEntityDescription insertNewObjectForEntityForName:[CDGitHubUser className] inManagedObjectContext:context];
    }
    
    cdUser.identification = (int)user.ID;
    cdUser.login = user.login;
    cdUser.avatarUrl = user.avatarURLString;
    
    user.cdGitHubUser = cdUser;
}

+ (void)deleteUser:(GitHubUser *)user inContext:(NSManagedObjectContext *)context {
    if (user.cdGitHubUser != nil) {
        [context deleteObject:user.cdGitHubUser];
    }
}

+ (NSArray<GitHubUser *> *)repositoriesInContext:(NSManagedObjectContext *)context {
    return [self findBy:nil inContext:context];
}

+ (GitHubUser *)findByID:(NSInteger)ID inContext:(NSManagedObjectContext *)context {
    return [[self findBy:[NSPredicate predicateWithFormat:@"identification == %d", (int)ID] inContext:context] firstObject];
}

+ (NSArray<GitHubUser *> *)findBy:(NSPredicate *)predicate inContext:(NSManagedObjectContext *)context {
    NSFetchRequest *request = [CDGitHubUser fetchRequest];
    request.predicate = predicate;
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    
    __block NSMutableArray<GitHubUser *> *users = [NSMutableArray<GitHubUser *> array];
    [results enumerateObjectsUsingBlock:^(CDGitHubUser *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [users addObject:[[GitHubUser alloc] initWithCDGitHubUser:obj]];
    }];
    
    return [NSArray<GitHubUser *> arrayWithArray:users];
}

#pragma mark -
#pragma mark Notifications

- (void)receiveUpdateAvatarNotification:(NSNotification *)notification {
    if ([notification.object integerValue] == self.ID) {
        self.avatarImage.value = [UIImage imageWithData:self.cdGitHubUser.avatar];
    }
}

@end
