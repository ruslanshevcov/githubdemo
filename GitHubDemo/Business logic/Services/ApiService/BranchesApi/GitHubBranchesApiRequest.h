//
//  GitHubBranchesApiRequest.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/25/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "GitHubApiRequest.h"

@class GitHubRepository;

@interface GitHubBranchesApiRequest : GitHubApiRequest

- (instancetype)initWith:(GitHubRepository *)repository;

@end
