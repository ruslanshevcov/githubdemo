//
//  GitHubBranchesApiRequest.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/25/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "GitHubBranchesApiRequest.h"
#import "GitHubBranchesApiResponse.h"
#import "GitHubRepository.h"
#import "GitHubUser.h"

@interface GitHubBranchesApiRequest ()

@property (nonatomic, strong) GitHubRepository *repository;

@end

@implementation GitHubBranchesApiRequest

- (instancetype)initWith:(GitHubRepository *)repository {
    if ([super init] == nil) {
        return nil;
    }
    
    self.repository = repository;
    
    return self;
}

- (NSMutableURLRequest *)urlRequest {
    
    NSString *targetUrl = [NSString stringWithFormat:@"%@/repos/%@/%@/branches", [self baseUrl], self.repository.owner.login, self.repository.name];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:targetUrl]];
    return request;
}

- (void)processingResponce:(NSURLResponse *)urlResponce data:(NSData *)responceData error:(NSError *)error {
    
    if (error != nil) {
        self.completion(nil, &error);
        return;
    }
    
    GitHubBranchesApiResponse *gitHubBranchesApiResponse = [[GitHubBranchesApiResponse alloc] initWithJSONData:responceData forRepository:self.repository];
    
    self.completion(gitHubBranchesApiResponse, nil);
}

@end
