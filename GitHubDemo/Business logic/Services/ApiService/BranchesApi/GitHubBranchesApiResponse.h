//
//  GitHubBranchesApiResponse.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/25/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "GitHubApiResponse.h"
#import "GitHubBranch.h"

@class GitHubRepository;

@interface GitHubBranchesApiResponse : GitHubApiResponse

@property (readonly) NSArray<GitHubBranch *> *branches;

- (instancetype)initWithJSONData:(NSData *)jsonData forRepository:(GitHubRepository *)repository;

@end
