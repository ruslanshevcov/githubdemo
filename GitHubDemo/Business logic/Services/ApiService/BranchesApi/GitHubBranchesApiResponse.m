//
//  GitHubBranchesApiResponse.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/25/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "GitHubBranchesApiResponse.h"
#import "GitHubRepository.h"

@interface GitHubBranchesApiResponse ()

@property (nonatomic, strong) NSArray<GitHubBranch *> *branches;

@end

@implementation GitHubBranchesApiResponse

- (instancetype)initWithJSONData:(NSData *)jsonData forRepository:(GitHubRepository *)repository {
    if ([super init] == nil) {
        return nil;
    }
    
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
    
    if ([jsonArray isKindOfClass:[NSArray class]] == NO) {
        return nil;
    }
    
    __block NSMutableArray *branches = [NSMutableArray array];
    [jsonArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        [branches addObject: [[GitHubBranch alloc] initWithJSONData: obj forRepository:repository]];
        
    }];
    
    self.branches = [NSArray arrayWithArray:branches];
    
    return self;
}

@end
