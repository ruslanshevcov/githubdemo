//
//  GitHubCommitsApiRequest.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/25/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "GitHubApiRequest.h"

@class GitHubBranch;
@class GitHubRepository;

@interface GitHubCommitsApiRequest : GitHubApiRequest

- (instancetype)initWith:(GitHubRepository *)repository forBranch:(GitHubBranch *)branch;

@end
