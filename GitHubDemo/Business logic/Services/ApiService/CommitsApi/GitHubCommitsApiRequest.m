//
//  GitHubCommitsApiRequest.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/25/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "GitHubCommitsApiRequest.h"
#import "GitHubCommitsApiResponse.h"
#import "GitHubRepository.h"
#import "GitHubBranch.h"
#import "GitHubUser.h"

@interface GitHubCommitsApiRequest ()

@property (nonatomic, strong) GitHubRepository *repository;
@property (nonatomic, strong) GitHubBranch *branch;

@end

@implementation GitHubCommitsApiRequest

- (instancetype)initWith:(GitHubRepository *)repository forBranch:(GitHubBranch *)branch {
    if ([super init] == nil) {
        return nil;
    }
    
    self.repository = repository;
    self.branch = branch;
    
    return self;
}

- (NSMutableURLRequest *)urlRequest {
    
    NSString *targetUrl = [NSString stringWithFormat:@"%@/repos/%@/%@/commits?sha=%@", [self baseUrl], self.repository.owner.login, self.repository.name, self.branch.headCommitSha];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:targetUrl]];
    return request;
}

- (void)processingResponce:(NSURLResponse *)urlResponce data:(NSData *)responceData error:(NSError *)error {
    
    if (error != nil) {
        self.completion(nil, &error);
        return;
    }
    
    GitHubCommitsApiResponse *gitHubCommitsApiResponse = [[GitHubCommitsApiResponse alloc] initWithJSONData:responceData forBranch:self.branch];
    
    self.completion(gitHubCommitsApiResponse, nil);
}

@end
