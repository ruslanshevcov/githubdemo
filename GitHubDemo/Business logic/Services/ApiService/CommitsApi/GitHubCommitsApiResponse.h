//
//  GitHubCommitsApiResponse.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/25/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "GitHubApiResponse.h"

@class GitHubCommit;
@class GitHubBranch;

@interface GitHubCommitsApiResponse : GitHubApiResponse

@property (readonly) NSArray<GitHubCommit *> *commits;

- (instancetype)initWithJSONData:(NSData *)jsonData forBranch:(GitHubBranch *)branch;

@end
