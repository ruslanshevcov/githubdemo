//
//  GitHubCommitsApiResponse.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/25/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "GitHubCommitsApiResponse.h"
#import "GitHubCommit.h"
#import "GitHubBranch.h"

@interface GitHubCommitsApiResponse ()

@property (nonatomic, strong) NSArray<GitHubCommit *> *commits;

@end

@implementation GitHubCommitsApiResponse

- (instancetype)initWithJSONData:(NSData *)jsonData forBranch:(GitHubBranch *)branch {
    if ([super init] == nil) {
        return nil;
    }
    
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
    
    if ([jsonArray isKindOfClass:[NSArray class]] == NO) {
        return nil;
    }
    
    __block NSMutableArray *commits = [NSMutableArray array];
    [jsonArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        [commits addObject: [[GitHubCommit alloc] initWithJSONData: obj forBranch:branch]];
        
    }];

    self.commits = [NSArray arrayWithArray:commits];
    
    return self;
}

@end
