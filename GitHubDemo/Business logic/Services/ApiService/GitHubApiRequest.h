//
//  GitHubApiRequest.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/24/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GitHubApiResponse.h"

@interface GitHubApiRequest : NSObject

typedef void (^GitHubApiRequestCompletion)(GitHubApiResponse *response, NSError **error);
@property (nonatomic, copy) GitHubApiRequestCompletion completion;

- (NSString *)baseUrl;
- (NSMutableURLRequest *)urlRequest;
- (void)processingResponce:(NSURLResponse *)urlResponce data:(NSData *)responceData error:(NSError *)error;

@end
