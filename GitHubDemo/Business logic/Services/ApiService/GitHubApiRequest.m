//
//  GitHubApiRequest.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/24/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "GitHubApiRequest.h"

static NSString *const kBaseURL = @"https://api.github.com";

@interface GitHubApiRequest ()

@end

@implementation GitHubApiRequest

- (NSString *)baseUrl {
    return kBaseURL;
}

- (NSMutableURLRequest *)urlRequest {
    return nil;
}

- (void)processingResponce:(NSURLResponse *)urlResponce data:(NSData *)responceData error:(NSError *)error {
    
}

@end
