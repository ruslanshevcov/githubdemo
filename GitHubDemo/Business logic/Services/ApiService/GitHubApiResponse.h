//
//  GitHubApiResponse.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/25/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GitHubApiResponse : NSObject

- (instancetype)initWithJSONData:(NSData *)jsonData;

@end
