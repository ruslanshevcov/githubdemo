//
//  GitHubApiService.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/21/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GitHubApiRequest.h"

@interface GitHubApiService : NSObject

- (void)doRequest:(GitHubApiRequest *)gitHubApiRequest;

@end
