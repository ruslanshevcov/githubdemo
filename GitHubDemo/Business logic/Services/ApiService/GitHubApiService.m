//
//  GitHubApiService.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/21/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "GitHubApiService.h"

@interface GitHubApiService ()

@end

@implementation GitHubApiService

- (void)doRequest:(GitHubApiRequest *)gitHubApiRequest {
    
    GitHubApiRequest *request = gitHubApiRequest;
    
    if (request == nil) {
        return;
    }
    
    NSMutableURLRequest *urlRequest = request.urlRequest;
    
    if (urlRequest == nil) {
        request.completion(nil, nil);
        return;
    }
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:urlRequest completionHandler:
      ^(NSData * _Nullable data,
        NSURLResponse * _Nullable response,
        NSError * _Nullable error) {
          [request processingResponce:response data:data error:error];
      }] resume];
}

@end
