//
//  GitHubApiRepositoriesRequest.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/24/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "GitHubApiRequest.h"

@interface GitHubApiRepositoriesRequest : GitHubApiRequest

@end
