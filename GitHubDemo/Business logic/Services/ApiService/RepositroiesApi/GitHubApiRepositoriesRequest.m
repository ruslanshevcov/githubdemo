//
//  GitHubApiRepositoriesRequest.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/24/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "GitHubApiRepositoriesRequest.h"
#import "GitHubRepositoriesApiResponse.h"

@interface GitHubApiRepositoriesRequest ()

@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) NSInteger pagesCount;

@end

@implementation GitHubApiRepositoriesRequest

- (NSMutableURLRequest *)urlRequest {
    
    if (self.currentPage > 0 && self.currentPage > self.pagesCount) {
        return nil;
    }
    
    self.currentPage++;
    
    NSString *targetUrl = [NSString stringWithFormat:@"%@/search/repositories?q=language:swift&per_page=20&page=%ld&sort=stars&order=desc", [self baseUrl], (long)self.currentPage];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:targetUrl]];
    return request;
}

- (void)processingResponce:(NSURLResponse *)urlResponce data:(NSData *)responceData error:(NSError *)error {
    
    if (error != nil) {
        self.pagesCount = 0;
        self.completion(nil, &error);
        return;
    }
    
    GitHubRepositoriesApiResponse *gitHubRepositoriesResponse = [[GitHubRepositoriesApiResponse alloc] initWithJSONData:responceData];

    if (self.pagesCount == 0) {
        self.pagesCount = gitHubRepositoriesResponse.repositoriesCount / 20;
    }
    
    if (self.pagesCount > 1000) {
      self.pagesCount = 1000 / 20;
    }

    if (self.pagesCount == 0) {
        self.currentPage = 0;
    }
    
    self.completion(gitHubRepositoriesResponse, nil);
}

@end
