//
//  GitHubRepositoriesApiResponse.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/21/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GitHubApiResponse.h"
#import "GitHubRepository.h"

@interface GitHubRepositoriesApiResponse : GitHubApiResponse

@property (readonly) NSInteger repositoriesCount;
@property (readonly) NSArray<GitHubRepository *> *repositories;

@end
