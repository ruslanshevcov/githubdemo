//
//  GitHubRepositoriesApiResponse.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/21/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "GitHubRepositoriesApiResponse.h"

static NSString *const kPagesCountKey = @"total_count";
static NSString *const kItemsKey = @"items";

@interface GitHubRepositoriesApiResponse ()

@property (nonatomic, assign) NSInteger repositoriesCount;
@property (nonatomic, strong) NSArray<GitHubRepository *> *repositories;

@end

@implementation GitHubRepositoriesApiResponse

- (instancetype)initWithJSONData:(NSData *)jsonData {
    if ([super init] == nil) {
        return nil;
    }
    
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
    
    self.repositoriesCount = [jsonDict[kPagesCountKey] integerValue];
    
    NSArray *repositoriesJSON = jsonDict[kItemsKey];
    
    __block NSMutableArray *repositories = [NSMutableArray array];
    
    [repositoriesJSON enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        [repositories addObject: [[GitHubRepository alloc] initWithJSONData: obj]];
        
    }];
    
    self.repositories = [NSArray arrayWithArray:repositories];
    
    return self;
}

@end
