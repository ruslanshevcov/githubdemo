//
//  RepositoriesListRouter.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/21/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GitHubRepository;
@class CoreDataService;

@interface RepositoriesListRouter : NSObject

- (void)openRepositoryDetailsFrom:(UIViewController *)view with:(GitHubRepository *)repository coreDataService:(CoreDataService *)coreDataService;

@end
