//
//  RepositoriesListRouter.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/21/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "RepositoriesListRouter.h"
#import "RepositoryDetailsViewController.h"
#import "RepositoryDetailsViewModel.h"
#import "RepositoryDataStorage.h"
#import "NSObject+ClassName.h"
#import "GitHubRepository.h"
#import "CoreDataService.h"

@implementation RepositoriesListRouter

- (void)openRepositoryDetailsFrom:(UIViewController *)view with:(GitHubRepository *)repository coreDataService:(CoreDataService *)coreDataService {
    
    UINavigationController *navigationController = view.navigationController;
    
    if (navigationController == nil) {
        return;
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"RootStoryboard" bundle:nil];
    RepositoryDetailsViewController *nextView = [storyboard instantiateViewControllerWithIdentifier:[RepositoryDetailsViewController className]];
    RepositoryDataStorage *repositoryDataStorage = [[RepositoryDataStorage alloc] initWith:repository coreDataService:coreDataService];
    nextView.viewModel = [[RepositoryDetailsViewModel alloc] initWith:repositoryDataStorage];
    [navigationController pushViewController:nextView animated:YES];
}

@end
