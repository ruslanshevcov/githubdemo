//
//  CommitTableViewModel.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/24/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RepositoryCellViewModel.h"
#import "Observable.h"

@class GitHubCommit;

@interface CommitTableViewModel : NSObject

@property (readonly) Observable<NSString *> *commitName;
@property (readonly) Observable<NSString *> *branchName;
@property (readonly) Observable<NSString *> *authorName;
@property (readonly) Observable<NSString *> *sha;
@property (readonly) Observable<UIImage *> *image;

- (instancetype)initWithCommit:(GitHubCommit *)commit;

@end
