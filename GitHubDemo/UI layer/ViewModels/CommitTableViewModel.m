//
//  CommitTableViewModel.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/24/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "CommitTableViewModel.h"
#import "GitHubUser.h"
#import "GitHubCommit.h"
#import "GitHubBranch.h"

@interface CommitTableViewModel ()

@property (nonatomic, strong) GitHubCommit *commit;
@property (nonatomic, strong) Observable<NSString *> *commitName;
@property (nonatomic, strong) Observable<NSString *> *branchName;
@property (nonatomic, strong) Observable<NSString *> *authorName;
@property (nonatomic, strong) Observable<NSString *> *sha;
@property (nonatomic, strong) Observable<UIImage *> *image;

@property (nonatomic, strong) NSMutableArray<Disposable *> *disposables;

@end

@implementation CommitTableViewModel

- (instancetype)initWithCommit:(GitHubCommit *)commit {
    if ([super init] == nil) {
        return nil;
    }
    
    self.disposables = [NSMutableArray<Disposable *> array];
    
    self.commit = commit;
    self.commitName = [[Observable<NSString *> alloc] initWithValue:commit.name];
    self.branchName = [[Observable<NSString *> alloc] initWithValue:commit.branch.name];
    self.authorName = [[Observable<NSString *> alloc] initWithValue:commit.authorName];
    self.sha = [[Observable<NSString *> alloc] initWithValue:commit.sha];
    
    __weak CommitTableViewModel *weakSelf = self;
    self.image = [Observable<UIImage *> new];
    [self.disposables addObject:[commit.author.avatarImage observe:^(UIImage *value, UIImage *oldValue) {
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.image.value = value;
        });
    }]];
    
    return self;
}

- (void)dealloc {
    [self.disposables removeAllObjects];
}

@end
