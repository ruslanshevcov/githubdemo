//
//  RepositoriesListViewModel.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/21/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RepositoriesDataStorage.h"
#import "RepositoryCellViewModel.h"
#import "Observable.h"

@interface RepositoriesListViewModel : NSObject

typedef void (^ReloadTableView)(void);
@property (nonatomic, copy) ReloadTableView reloadTableViewBlock;

@property (readonly) Observable<NSString *> *screenTitle;
@property (readonly) Observable<NSNumber *> *insertRowsCount;

- (instancetype)initWith:(RepositoriesDataStorage *)dataStorage;

- (void)load;
- (void)reload;

- (NSInteger)numberOfRepositories;
- (RepositoryCellViewModel *)cellViewModelAtIndex:(NSInteger)index;
- (void)openDetailsForRepositoryAtIndex:(NSInteger)index from:(UIViewController *)view;

@end
