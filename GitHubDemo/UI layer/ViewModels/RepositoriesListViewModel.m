//
//  RepositoriesListViewModel.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/21/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "RepositoriesListViewModel.h"
#import "RepositoriesListRouter.h"

@interface RepositoriesListViewModel ()

@property (nonatomic, strong) RepositoriesDataStorage *dataStorage;
@property (nonatomic, strong) RepositoriesListRouter *router;
@property (nonatomic, assign) BOOL processingRequest;
@property (nonatomic, strong) Observable<NSString *> *screenTitle;
@property (nonatomic, strong) Observable<NSNumber *> *insertRowsCount;

@end

@implementation RepositoriesListViewModel

- (instancetype)initWith:(RepositoriesDataStorage *)dataStorage {
    if ([super init] == nil) {
        return nil;
    }
    
    self.dataStorage = dataStorage;
    self.router = [RepositoriesListRouter new];
    self.screenTitle = [[Observable<NSString *> alloc] initWithValue:@"Repositories"];
    
    self.insertRowsCount = [[Observable<NSNumber *> alloc] init];
    self.insertRowsCount.value = @(self.dataStorage.repositories.count);
    
    return self;
}

- (void)load {
    if (self.processingRequest == YES) {
        return;
    }
    self.processingRequest = YES;
    __weak RepositoriesListViewModel *weakSelf = self;
    [self.dataStorage getRepositoriesWithCompletion:^() {
        weakSelf.insertRowsCount.value = @(weakSelf.dataStorage.repositories.count);
        weakSelf.processingRequest = NO;
    }];
}

- (void)reload {
    [self.dataStorage refresh];
    self.reloadTableViewBlock();
    self.insertRowsCount.value = @(0);
    [self load];
}

- (NSInteger)numberOfRepositories {
    return self.dataStorage.repositories.count;
}

- (RepositoryCellViewModel *)cellViewModelAtIndex:(NSInteger)index {
    return [[RepositoryCellViewModel alloc] initWithRepository:self.dataStorage.repositories[index]];
}

- (void)openDetailsForRepositoryAtIndex:(NSInteger)index from:(UIViewController *)view {
    [self.router openRepositoryDetailsFrom:view with:self.dataStorage.repositories[index] coreDataService:self.dataStorage.coreDataService];
}

@end
