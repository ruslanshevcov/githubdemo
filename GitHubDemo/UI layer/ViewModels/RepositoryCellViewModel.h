//
//  RepositoryCellViewModel.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/21/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Observable.h"

@class GitHubRepository;

@interface RepositoryCellViewModel : NSObject

@property (readonly) Observable<NSString *> *name;
@property (readonly) Observable<NSString *> *owner;
@property (readonly) Observable<NSString *> *desc;
@property (readonly) Observable<UIImage *> *image;

- (instancetype)initWithRepository:(GitHubRepository *)repository;

@end
