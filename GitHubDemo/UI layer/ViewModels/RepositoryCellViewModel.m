//
//  RepositoryCellViewModel.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/21/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "RepositoryCellViewModel.h"
#import "GitHubUser.h"
#import "GitHubRepository.h"

@interface RepositoryCellViewModel ()

@property (nonatomic, strong) Observable<NSString *> *name;
@property (nonatomic, strong) Observable<NSString *> *owner;
@property (nonatomic, strong) Observable<NSString *> *desc;
@property (nonatomic, strong) GitHubRepository *repository;
@property (nonatomic, strong) Observable<UIImage *> *image;

@property (nonatomic, strong) NSMutableArray<Disposable *> *disposables;

@end

@implementation RepositoryCellViewModel

- (instancetype)initWithRepository:(GitHubRepository *)repository {
    if ([super init] == nil) {
        return nil;
    }
    
    self.disposables = [NSMutableArray<Disposable *> array];
    
    self.repository = repository;
    self.name = [[Observable<NSString *> alloc] initWithValue:repository.name];
    self.owner = [[Observable<NSString *> alloc] initWithValue:repository.owner.login];
    self.desc = [[Observable<NSString *> alloc] initWithValue:repository.desc];
    
    __weak RepositoryCellViewModel *weakSelf = self;
    self.image = [Observable<UIImage *> new];
    [self.disposables addObject:[repository.owner.avatarImage observe:^(UIImage *value, UIImage *oldValue) {
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.image.value = value;
        });
    }]];

    return self;
}

- (void)dealloc {
    [self.disposables removeAllObjects];
}

@end
