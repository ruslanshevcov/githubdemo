//
//  RepositoryDetailsViewModel.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/24/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RepositoryDataStorage.h"
#import "CommitTableViewModel.h"
#import "Observable.h"

@interface RepositoryDetailsViewModel : NSObject

@property (readonly) Observable<NSString *> *screenTitle;

typedef void (^ReloadTableView)(void);
@property (nonatomic, copy) ReloadTableView reloadTableViewBlock;

- (instancetype)initWith:(RepositoryDataStorage *)dataStorage;

- (void)load;

- (NSInteger)numberOfCommits;
- (CommitTableViewModel *)cellViewModelAtIndex:(NSInteger)index;

@end
