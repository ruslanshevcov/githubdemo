//
//  RepositoryDetailsViewModel.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/24/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "RepositoryDetailsViewModel.h"
#import "RepositoriesListRouter.h"
#import "CommitTableViewModel.h"
#import "GitHubBranch.h"
#import "GitHubCommit.h"
#import "GitHubRepository.h"

@interface RepositoryDetailsViewModel ()

@property (nonatomic, strong) RepositoryDataStorage *dataStorage;
@property (nonatomic, strong) Observable<NSString *> *screenTitle;

@end

@implementation RepositoryDetailsViewModel

- (instancetype)initWith:(RepositoryDataStorage *)dataStorage {
    if ([super init] == nil) {
        return nil;
    }
    
    self.dataStorage = dataStorage;
    self.screenTitle = [[Observable<NSString *> alloc] initWithValue:dataStorage.repository.name];
    
    return self;
}

- (void)load {
    __weak RepositoryDetailsViewModel *weakSelf = self;
    [self.dataStorage getCommitsWithCompletion:^{
        weakSelf.reloadTableViewBlock();
    }];
}

- (NSInteger)numberOfCommits {
    __block NSInteger numberOfCommits = 0;
    [self.dataStorage.repository.branches enumerateObjectsUsingBlock:^(GitHubBranch * _Nonnull branch, NSUInteger idx, BOOL * _Nonnull stop) {
        numberOfCommits += branch.commits.count;
    }];
    return numberOfCommits;
}

- (CommitTableViewModel *)cellViewModelAtIndex:(NSInteger)index {
    
    __block NSMutableArray<GitHubCommit *> *commits = [NSMutableArray<GitHubCommit *> array];
    [self.dataStorage.repository.branches enumerateObjectsUsingBlock:^(GitHubBranch * _Nonnull branch, NSUInteger idx, BOOL * _Nonnull stop) {
        [commits addObjectsFromArray:branch.commits];
    }];
    
    return [[CommitTableViewModel alloc] initWithCommit:commits[index]];
}

@end
