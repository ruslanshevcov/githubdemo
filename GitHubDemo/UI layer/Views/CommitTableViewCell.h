//
//  CommitTableViewCell.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/24/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommitTableViewModel.h"

@interface CommitTableViewCell : UITableViewCell

@property (nonatomic, strong) CommitTableViewModel *viewModel;

+ (NSString *)reuseIdentifier;

@end
