//
//  CommitTableViewCell.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/24/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "CommitTableViewCell.h"
#import "NSObject+ClassName.h"

@interface CommitTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *authorAvatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *authorNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *commitShaLabel;
@property (weak, nonatomic) IBOutlet UILabel *commitNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *branchLabel;

@property (nonatomic, strong) NSMutableArray<Disposable *> *disposables;

@end

@implementation CommitTableViewCell

- (void)setViewModel:(CommitTableViewModel *)viewModel {
    _viewModel = viewModel;
    
    if (self.disposables == nil) {
        self.disposables = [NSMutableArray<Disposable *> array];
    } else {
        [self.disposables removeAllObjects];
    }
    
    __weak CommitTableViewCell *weakSelf = self;
    
    [self.disposables addObject:[viewModel.authorName observe:^(NSString *value, NSString *oldValue) {
        weakSelf.authorNameLabel.text = value;
    }]];
    
    [self.disposables addObject:[viewModel.sha observe:^(NSString *value, NSString *oldValue) {
        weakSelf.commitShaLabel.text = value;
    }]];
    
    [self.disposables addObject:[viewModel.commitName observe:^(NSString *value, NSString *oldValue) {
        weakSelf.commitNameLabel.text = value;
    }]];
    
    [self.disposables addObject:[viewModel.branchName observe:^(NSString *value, NSString *oldValue) {
        weakSelf.branchLabel.text = value;
    }]];
    
    [self.disposables addObject:[viewModel.image observe:^(UIImage *value, NSString *oldValue) {
        weakSelf.authorAvatarImageView.image = value;
    }]];
}

- (void)dealloc {
    [self.disposables removeAllObjects];
}

+ (NSString *)reuseIdentifier {
    return [self className];
}

@end
