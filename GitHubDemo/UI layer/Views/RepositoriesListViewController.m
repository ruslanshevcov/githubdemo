//
//  RepositoriesListViewController.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/21/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "RepositoriesListViewController.h"
#import "RepositoriesListViewModel.h"
#import "GitHubApiService.h"

// UI
#import "RepositoryTableViewCell.h"

@interface RepositoriesListViewController ()

@property (nonatomic, strong) RepositoriesListViewModel *viewModel;
@property (nonatomic, strong) NSMutableArray<Disposable *> *disposables;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation RepositoriesListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.viewModel = [[RepositoriesListViewModel alloc] initWith:[[RepositoriesDataStorage alloc] init]];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    self.tableView.refreshControl = self.refreshControl;
    
    __weak RepositoriesListViewController *weakSelf = self;
    self.viewModel.reloadTableViewBlock = ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.tableView reloadData];
        });
    };
    
    self.disposables = [NSMutableArray<Disposable *> array];
    [self.disposables addObject:[self.viewModel.screenTitle observe:^(NSString *value, NSString *oldValue) {
        weakSelf.title = value;
    }]];
    
    [self.disposables addObject:[self.viewModel.insertRowsCount observe:^(NSNumber *value, NSNumber *oldValue) {
        
        if (value == nil) {
            return;
        }
        
        NSMutableArray <NSIndexPath *> *rows = [NSMutableArray <NSIndexPath *> array];
        for (int index = [oldValue intValue]; index < [value intValue]; index++) {
            [rows addObject:[NSIndexPath indexPathForRow:index inSection:0]];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.tableView beginUpdates];
            [weakSelf.tableView insertRowsAtIndexPaths:rows withRowAnimation:YES];
            [weakSelf.tableView endUpdates];
        });
        
    }]];
    
    [self.tableView registerClass:[RepositoryTableViewCell class] forCellReuseIdentifier:[RepositoryTableViewCell reuseIdentifier]];
    [self.tableView registerNib:[UINib nibWithNibName:[RepositoryTableViewCell reuseIdentifier] bundle:nil] forCellReuseIdentifier:[RepositoryTableViewCell reuseIdentifier]];
    
    [self.viewModel load];
}

- (void)dealloc {
    [self.disposables removeAllObjects];
}

#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.viewModel numberOfRepositories];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    RepositoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: [RepositoryTableViewCell reuseIdentifier]];
    cell.viewModel = [self.viewModel cellViewModelAtIndex:indexPath.row];
    
    if (indexPath.row == ([self.viewModel numberOfRepositories] - 1)) {
        [self.viewModel load];
    }
    
    return cell;
}

#pragma mark -
#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.viewModel openDetailsForRepositoryAtIndex:indexPath.row from:self];
}

#pragma mark -
#pragma mark Refresh action

- (void)refreshTable {
    [self.refreshControl endRefreshing];
    [self.viewModel reload];
}

@end
