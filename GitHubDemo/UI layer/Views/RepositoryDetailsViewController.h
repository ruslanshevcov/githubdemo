//
//  RepositoryDetailsViewController.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/24/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RepositoryDetailsViewModel.h"

@interface RepositoryDetailsViewController : UIViewController

@property (nonatomic, strong) RepositoryDetailsViewModel *viewModel;

@end
