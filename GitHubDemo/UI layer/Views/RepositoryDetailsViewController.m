//
//  RepositoryDetailsViewController.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/24/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "RepositoryDetailsViewController.h"
#import "CommitTableViewCell.h"

@interface RepositoryDetailsViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray<Disposable *> *disposables;

@end

@implementation RepositoryDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass:[CommitTableViewCell class] forCellReuseIdentifier:[CommitTableViewCell reuseIdentifier]];
    [self.tableView registerNib:[UINib nibWithNibName:[CommitTableViewCell reuseIdentifier] bundle:nil] forCellReuseIdentifier:[CommitTableViewCell reuseIdentifier]];
    
    __weak RepositoryDetailsViewController *weakSelf = self;
    self.viewModel.reloadTableViewBlock = ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.tableView reloadData];
        });
    };
    
    self.disposables = [NSMutableArray<Disposable *> array];
    [self.disposables addObject:[self.viewModel.screenTitle observe:^(NSString *value, NSString *oldValue) {
        weakSelf.title = value;
    }]];
    
    [self.viewModel load];
}

- (void)dealloc {
    [self.disposables removeAllObjects];
}

#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.viewModel numberOfCommits];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    CommitTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: [CommitTableViewCell reuseIdentifier]];
    cell.viewModel = [self.viewModel cellViewModelAtIndex:indexPath.row];
    return cell;
}

@end
