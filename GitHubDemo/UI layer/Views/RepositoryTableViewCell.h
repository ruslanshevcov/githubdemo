//
//  RepositoryTableViewCell.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/21/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RepositoryCellViewModel.h"

@interface RepositoryTableViewCell : UITableViewCell

@property (nonatomic, strong) RepositoryCellViewModel *viewModel;

+ (NSString *)reuseIdentifier;

@end
