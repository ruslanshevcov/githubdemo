//
//  RepositoryTableViewCell.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/21/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "RepositoryTableViewCell.h"
#import "NSObject+ClassName.h"
#import "GitHubUser.h"
#import "Observable.h"

@interface RepositoryTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *ownerLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;

@property (nonatomic, strong) NSMutableArray<Disposable *> *disposables;

@end

@implementation RepositoryTableViewCell

- (void)setViewModel:(RepositoryCellViewModel *)viewModel {
    _viewModel = viewModel;
    
    if (self.disposables == nil) {
        self.disposables = [NSMutableArray<Disposable *> array];
    } else {
        [self.disposables removeAllObjects];
    }
    
    __weak RepositoryTableViewCell *weakSelf = self;
    
    [self.disposables addObject:[viewModel.name observe:^(NSString *value, NSString *oldValue) {
        weakSelf.nameLabel.text = value;
    }]];
    
    [self.disposables addObject:[viewModel.owner observe:^(NSString *value, NSString *oldValue) {
        weakSelf.ownerLabel.text = value;
    }]];
    
    [self.disposables addObject:[viewModel.desc observe:^(NSString *value, NSString *oldValue) {
        weakSelf.descLabel.text = value;
    }]];
    
    [self.disposables addObject:[viewModel.image observe:^(UIImage *value, NSString *oldValue) {
        weakSelf.avatarImageView.image = value;
    }]];
}

- (void)dealloc {
    [self.disposables removeAllObjects];
}

+ (NSString *)reuseIdentifier {
    return [self className];
}

@end
