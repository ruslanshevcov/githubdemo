//
//  Macro.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/26/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#define CHECK_NSNULL(obj) (![obj isEqual:[NSNull null]] ? obj : nil)
