//
//  NSObject+ClassName.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/24/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (ClassName)

+ (NSString *)className;

@end
