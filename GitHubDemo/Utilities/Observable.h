//
//  Observable.h
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/26/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Disposable;

@interface Observable <ValueType> : NSObject

@property (nonatomic, strong) ValueType value;

- (instancetype)initWithValue:(ValueType)value;

typedef void (^Observer)(ValueType value, ValueType oldValue);
- (Disposable *)observe:(Observer)observer;

@end

@interface Disposable : NSObject

typedef void (^Dispose)(void);
- (instancetype)initWith:(Dispose)dispose;

@end
