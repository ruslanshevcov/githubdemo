//
//  Observable.m
//  GitHubDemo
//
//  Created by Ruslan Shevtsov on 6/26/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "Observable.h"

@interface Observable()

@property (nonatomic, strong) NSMutableDictionary<NSNumber *, Observer> *observers;
@property (nonatomic, strong) NSNumber *iterator;

@end

@implementation Observable 

- (instancetype)init {
    if ([super init] == nil) {
        return nil;
    }
    
    self.observers = [NSMutableDictionary<NSNumber *, Observer> dictionary];
    self.iterator = @0;
    
    return self;
}

- (instancetype)initWithValue:(id)value {
    if ([super init] == nil) {
        return nil;
    }
    
    self.observers = [NSMutableDictionary<NSNumber *, Observer> dictionary];
    self.iterator = @0;
    
    self.value = value;
    
    return self;
}

- (void)setValue:(id)value {
    id oldValue = _value;
    _value = value;
    
    [self.observers.allKeys enumerateObjectsUsingBlock:^(NSNumber * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        self.observers[obj](value, oldValue);
    }];
}

- (Disposable *)observe:(Observer)observer {
    [self.observers setObject:observer forKey:self.iterator];
    
    NSNumber *key = self.iterator;
    
    self.iterator = @([self.iterator intValue] + 1);
    observer(self.value, nil);
    
    __weak Observable *weakSelf = self;
    Disposable *disposable = [[Disposable alloc] initWith:^{
        [weakSelf.observers removeObjectForKey:key];
    }];
    
    return disposable;
}

@end

@interface Disposable ()

@property (nonatomic, copy) Dispose dispose;

@end

@implementation Disposable

- (instancetype)initWith:(Dispose)dispose {
    if ([super init] == nil) {
        return nil;
    }
    
    self.dispose = dispose;
    
    return self;
}

- (void)dealloc {
    self.dispose();
}

@end
